package com.bridgelabz.bookstore.dto;

import lombok.Data;

@Data
public class AddressDto {
		private String address;
		private String locality;
		private String name;
		private String city;
		private String landmark;
		private String state;
		private String pincode;
		private String country;
		private String type;
		private String phoneNumber;
		public String getName() {
			// TODO Auto-generated method stub
			return null;
		}
		public String getPhoneNumber() {
			// TODO Auto-generated method stub
			return null;
		}
		public String getLandmark() {
			// TODO Auto-generated method stub
			return null;
		}
		public String getType() {
			// TODO Auto-generated method stub
			return null;
		}
		public String getPincode() {
			// TODO Auto-generated method stub
			return null;
		}
		public String getState() {
			// TODO Auto-generated method stub
			return null;
		}
		public String getCountry() {
			// TODO Auto-generated method stub
			return null;
		}
		public String getAddress() {
			// TODO Auto-generated method stub
			return null;
		}
		public String getLocality() {
			// TODO Auto-generated method stub
			return null;
		}
		public void setCity(String string) {
			// TODO Auto-generated method stub
			
		}
		public String getCity() {
			return city;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		public void setLocality(String locality) {
			this.locality = locality;
		}
		public void setName(String name) {
			this.name = name;
		}
		public void setLandmark(String landmark) {
			this.landmark = landmark;
		}
		public void setState(String state) {
			this.state = state;
		}
		public void setPincode(String pincode) {
			this.pincode = pincode;
		}
		public void setCountry(String country) {
			this.country = country;
		}
		public void setType(String type) {
			this.type = type;
		}
		public void setPhoneNumber(String phoneNumber) {
			this.phoneNumber = phoneNumber;
		}
		@Override
		public String toString() {
			return "AddressDto [address=" + address + ", locality=" + locality + ", name=" + name + ", city=" + city
					+ ", landmark=" + landmark + ", state=" + state + ", pincode=" + pincode + ", country=" + country
					+ ", type=" + type + ", phoneNumber=" + phoneNumber + "]";
		}
		
}